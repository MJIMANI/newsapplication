# News Application
Simple news application and user management system 

## Technologies
Project has created with:
* Java 17
* Spring Boot 2.6.3
* Spring Data
* MongoDB
* Maven