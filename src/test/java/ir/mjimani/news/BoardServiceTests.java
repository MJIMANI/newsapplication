package ir.mjimani.news;

import ir.mjimani.news.dao.BoardDao;
import ir.mjimani.news.domain.Board;
import ir.mjimani.news.service.BoardService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
public class BoardServiceTests {

    @MockBean
    private BoardDao boardDao;
    @Autowired
    private BoardService boardService;

    @Test
    void getBoard() {
        // given
        Board givenBoard = new Board();
        givenBoard.setBoardName("To-Qa");
        givenBoard.setId("GivenBoardId");

        when(boardDao.getBoard(givenBoard.getId())).thenReturn(givenBoard);

        // when
        Board resultBoard = boardService.getBoard(givenBoard.getId());

        // then
        assertThat(resultBoard.getId()).isEqualTo(givenBoard.getId());
        assertThat(resultBoard.getBoardName()).isEqualTo(givenBoard.getBoardName());
    }

    @Test
    void deleteBoard() {
        // given
        Board givenBoard = new Board();
        givenBoard.setId("GivenBoardId");

        when(boardDao.deleteBoard(givenBoard.getId())).thenReturn(true);

        // when
        boolean result = boardService.deleteBoard(givenBoard.getId());

        // then
        assertThat(result).isEqualTo(true);
    }


}
