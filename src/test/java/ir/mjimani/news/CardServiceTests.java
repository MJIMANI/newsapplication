package ir.mjimani.news;

import ir.mjimani.news.controller.dto.card.CardListReqDto;
import ir.mjimani.news.dao.CardDao;
import ir.mjimani.news.domain.Card;
import ir.mjimani.news.service.CardService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CardServiceTests {

    @MockBean
    private CardDao cardDao;
    @Autowired
    private CardService cardService;

    @Test
    void getCard() {
        // given
        Card givenCard = new Card();
        givenCard.setId("GivenCardId");
        givenCard.setBoardId("GivenBoardId");
        givenCard.setCardTitle("GivenCardTitle");

        when(cardDao.getCard(givenCard.getBoardId(), givenCard.getId())).thenReturn(givenCard);

        // when
        Card resultCard = cardService.getCard(givenCard.getBoardId(), givenCard.getId());

        // then
        assertThat(resultCard.getId()).isEqualTo(givenCard.getId());
        assertThat(resultCard.getBoardId()).isEqualTo(givenCard.getBoardId());
        assertThat(resultCard.getCardTitle()).isEqualTo(givenCard.getCardTitle());
    }

    @Test
    void search_cards_with_title() {
        // given
        CardListReqDto cardListReqDto = new CardListReqDto();
        cardListReqDto.setCardTitle("GivenCardTitle");
        int page = 0;
        int size = 10;

        Card givenCard1 = new Card();
        givenCard1.setId("GivenCardId1");
        givenCard1.setBoardId("GivenBoardId1");
        givenCard1.setCardTitle("GivenCardTitle1");

        Card givenCard2 = new Card();
        givenCard2.setId("GivenCardId2");
        givenCard2.setBoardId("GivenBoardId1");
        givenCard2.setCardTitle("GivenCardTitle2");

        List<Card> cards = Arrays.asList(givenCard1, givenCard2);
        Pageable pageable = PageRequest.of(page, size);
        int total = 2;
        PageImpl<Card> cardPage = new PageImpl<>(cards, pageable, total);

        when(cardDao.getCards(givenCard1.getBoardId(), cardListReqDto, page, size)).thenReturn(cardPage);

        // when
        Page<Card> cardPageResult = cardService.getCards(givenCard1.getBoardId(), cardListReqDto, page, size);
        Card card = cardPageResult.getContent().get(0);

        // then
        assertThat(cardPageResult.getTotalElements()).isEqualTo(total);
        assertThat(card.getId()).isEqualTo(givenCard1.getId());
        assertThat(card.getCardTitle()).isEqualTo(givenCard1.getCardTitle());
    }

}
