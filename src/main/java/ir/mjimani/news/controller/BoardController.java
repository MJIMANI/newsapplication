package ir.mjimani.news.controller;

import ir.mjimani.news.base.Response;
import ir.mjimani.news.controller.dto.board.CreateBoardReqDto;
import ir.mjimani.news.controller.dto.board.CreateBoardResDto;
import ir.mjimani.news.controller.dto.board.UpdateBoardReqDto;
import ir.mjimani.news.domain.Board;
import ir.mjimani.news.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/board")
@RequiredArgsConstructor
public class BoardController {

    private final BoardService boardService;

    @PostMapping
    public ResponseEntity<Response<CreateBoardResDto>> createBoard(@RequestBody @Valid CreateBoardReqDto createBoardReqDto) {
        return ResponseEntity.ok().body(new Response<>(boardService.createBoard(createBoardReqDto)));
    }

    @GetMapping
    public ResponseEntity<Response<Page<Board>>> getList(@RequestParam(value = "page", defaultValue = "0") int page,
                                                         @RequestParam(value = "size", defaultValue = "10") int size) {
        return ResponseEntity.ok().body(new Response<>(boardService.getList(page, size)));
    }

    @GetMapping("/{boardId}")
    public ResponseEntity<Response<Board>> getBoard(@PathVariable String boardId) {
        return ResponseEntity.ok().body(new Response<>(boardService.getBoard(boardId)));
    }

    @PutMapping("/{boardId}")
    public ResponseEntity<Response<Boolean>> updateBoard(@PathVariable String boardId, @RequestBody @Valid UpdateBoardReqDto updateBoardReqDto) {
        return ResponseEntity.ok().body(new Response<>(boardService.updateBoard(boardId, updateBoardReqDto)));
    }

    @DeleteMapping("/{boardId}")
    public ResponseEntity<Response<Boolean>> deleteBoard(@PathVariable String boardId) {
        return ResponseEntity.ok().body(new Response<>(boardService.deleteBoard(boardId)));
    }
}
