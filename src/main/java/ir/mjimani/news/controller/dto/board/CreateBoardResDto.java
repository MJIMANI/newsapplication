package ir.mjimani.news.controller.dto.board;

import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateBoardResDto {
    private String boardId;
}
