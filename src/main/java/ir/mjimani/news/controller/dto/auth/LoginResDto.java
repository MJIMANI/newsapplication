package ir.mjimani.news.controller.dto.auth;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginResDto {
    private String id;
    private String username;
    private String accessToken;
    private Collection<? extends GrantedAuthority> roles;
    private String tokenType;
}
