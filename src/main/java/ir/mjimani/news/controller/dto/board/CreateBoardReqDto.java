package ir.mjimani.news.controller.dto.board;

import ir.mjimani.news.domain.Board;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CreateBoardReqDto {
    @NotNull
    @NotBlank
    private String boardName;

    public Board toBoard() {
        return Board
                .builder()
                .boardName(boardName)
                .build();
    }
}
