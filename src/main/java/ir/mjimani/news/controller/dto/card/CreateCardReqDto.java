package ir.mjimani.news.controller.dto.card;

import ir.mjimani.news.domain.Card;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CreateCardReqDto {

    @NotNull
    @NotBlank
    private String cardTitle;

    public Card toCard() {
        return Card
                .builder()
                .cardTitle(cardTitle)
                .build();
    }
}
