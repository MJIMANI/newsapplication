package ir.mjimani.news.controller.dto.card;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CardListReqDto {
    private String cardTitle = "";
    private String memberId = "";
    private Boolean sortByModifiedOn = false;
}
