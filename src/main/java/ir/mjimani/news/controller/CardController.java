package ir.mjimani.news.controller;

import ir.mjimani.news.base.Response;
import ir.mjimani.news.controller.dto.card.CardListReqDto;
import ir.mjimani.news.controller.dto.card.CreateCardReqDto;
import ir.mjimani.news.controller.dto.card.CreateCardResDto;
import ir.mjimani.news.controller.dto.card.UpdateCardReqDto;
import ir.mjimani.news.domain.Card;
import ir.mjimani.news.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/board/{boardId}/cards")
@RequiredArgsConstructor
public class CardController {

    private final CardService cardService;

    @PostMapping
    public ResponseEntity<Response<CreateCardResDto>> createCard(@PathVariable String boardId, @RequestBody @Valid CreateCardReqDto createCardReqDto) {
        return ResponseEntity.ok().body(new Response<>(cardService.createCard(boardId, createCardReqDto)));
    }

    @GetMapping("{cardId}")
    public ResponseEntity<Response<Card>> getCard(@PathVariable String boardId, @PathVariable String cardId) {
        return ResponseEntity.ok().body(new Response<>(cardService.getCard(boardId, cardId)));
    }

    @PutMapping("{cardId}")
    public ResponseEntity<Response<Boolean>> updateCard(@PathVariable String boardId, @PathVariable String cardId, @RequestBody @Valid UpdateCardReqDto updateCardReqDto) {
        return ResponseEntity.ok().body(new Response<>(cardService.updateCard(boardId, cardId, updateCardReqDto)));
    }

    @DeleteMapping("{cardId}")
    public ResponseEntity<Response<Boolean>> deleteCard(@PathVariable String boardId, @PathVariable String cardId) {
        return ResponseEntity.ok().body(new Response<>(cardService.deleteCard(boardId, cardId)));
    }

    @PutMapping("{cardId}/assign/{userId}")
    public ResponseEntity<Response<Boolean>> assignMember(@PathVariable String boardId, @PathVariable String cardId, @PathVariable String userId) {
        return ResponseEntity.ok().body(new Response<>(cardService.assignMember(boardId, cardId, userId)));
    }

    @PutMapping("{cardId}/un-assign/{userId}")
    public ResponseEntity<Response<Boolean>> unAssignMember(@PathVariable String boardId, @PathVariable String cardId, @PathVariable String userId) {
        return ResponseEntity.ok().body(new Response<>(cardService.unAssignMember(boardId, cardId, userId)));
    }

    @GetMapping
    public ResponseEntity<Response<Page<Card>>> getCards(@PathVariable String boardId,
                                                         CardListReqDto cardListReqDto,
                                                         @RequestParam(value = "page", defaultValue = "0") int page,
                                                         @RequestParam(value = "size", defaultValue = "10") int size) {
        return ResponseEntity.ok().body(new Response<>(cardService.getCards(boardId, cardListReqDto, page, size)));
    }
}
