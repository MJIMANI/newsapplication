package ir.mjimani.news.controller;

import ir.mjimani.news.base.Response;
import ir.mjimani.news.controller.dto.auth.LoginReqDto;
import ir.mjimani.news.controller.dto.auth.LoginResDto;
import ir.mjimani.news.controller.dto.auth.SignupReqDto;
import ir.mjimani.news.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth/")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("login")
    public ResponseEntity<Response<LoginResDto>> authenticateUser(@RequestBody @Valid LoginReqDto loginReqDto) {
        return ResponseEntity.ok().body(new Response<>(authService.login(loginReqDto)));
    }

    @PostMapping("signUp")
    public ResponseEntity<Response<Boolean>> registerUser(@RequestBody @Valid SignupReqDto signUpReqDto) {
        return ResponseEntity.ok().body(
                new Response<>(authService.registerUser(signUpReqDto))
                        .message("User registered successfully!"));
    }
}