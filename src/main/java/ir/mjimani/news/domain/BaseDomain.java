package ir.mjimani.news.domain;

import ir.mjimani.news.security.SecurityUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Setter
@Getter
public class BaseDomain {
    public enum BFN {
        id, createdOn, modifiedOn, creatorId
    }

    @Id
    protected String id;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    protected Date createdOn;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    protected Date modifiedOn;

    protected String creatorId;

    public void initCreatorIdAndCreatedDate() {
        this.createdOn = new Date();
        this.creatorId = SecurityUtils.getLoggedInUserId();
    }

}
