package ir.mjimani.news.domain;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "card")
public class Card extends BaseDomain {
    public enum FN {
        cardTitle, boardId, members
    }

    private String cardTitle;

    private String boardId;

    private List<String> members;
}
