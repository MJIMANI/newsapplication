package ir.mjimani.news.domain;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "board")
public class Board extends BaseDomain {
    public enum FN {
        boardName
    }

    private String boardName;
}
