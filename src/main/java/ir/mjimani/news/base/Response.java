package ir.mjimani.news.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response<T> {

    protected Boolean success;

    private T data;

    protected List<String> developerMessage;

    protected String message;

    public Response<T> data(T data) {
        this.data = data;
        return this;
    }

    public Response<T> success(Boolean success) {
        this.success = success;
        return this;
    }

    public Response<T> developerMessage(List<String> message) {
        this.developerMessage = message;
        this.success = false;
        return this;
    }

    public Response<T> message(String message) {
        this.message = message;
        return this;
    }

    public Response(T data) {
        success = true;
        this.data = data;
    }
}
