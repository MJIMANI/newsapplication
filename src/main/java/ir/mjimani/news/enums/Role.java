package ir.mjimani.news.enums;

public enum Role {
    ROLE_ADMIN,
    ROLE_USER
}
