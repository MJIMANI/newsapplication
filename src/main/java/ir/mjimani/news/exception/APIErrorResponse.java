package ir.mjimani.news.exception;

import ir.mjimani.news.base.Response;

import java.util.List;

public class APIErrorResponse extends Response<APIErrorResponse> {

    public APIErrorResponse(List<String> message) {
        success = false;
        this.message = message.get(0);
        this.developerMessage = message;
    }
}