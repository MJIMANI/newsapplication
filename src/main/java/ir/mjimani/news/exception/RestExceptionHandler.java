package ir.mjimani.news.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return buildResponseEntity(new APIErrorResponse(Collections.singletonList(ex.getMessage())), HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<Object> buildResponseEntity(APIErrorResponse apiError, HttpStatus httpStatus) {
        return ResponseEntity.status(httpStatus.value())
                .body(apiError);
    }

    @ExceptionHandler(AccessDeniedException.class)
    protected ResponseEntity<Object> handleAccessDeniedException(Exception ex) {
        APIErrorResponse apiError = new APIErrorResponse(Collections.singletonList(ex.getMessage()));
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(UnauthorizedException.class)
    protected ResponseEntity<Object> handleUnauthorizedException(UnauthorizedException ex) {
        APIErrorResponse apiError = new APIErrorResponse(Collections.singletonList(ex.getMessage()));
        return buildResponseEntity(apiError, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(APIException.class)
    protected ResponseEntity<Object> handleCustomException(APIException ex) {
        APIErrorResponse apiError = new APIErrorResponse(Collections.singletonList(ex.getMessage()));
        return buildResponseEntity(apiError, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(HttpServerErrorException.InternalServerError.class)
    protected ResponseEntity<Object> handleHttpServerErrorException$InternalServerError(Exception ex) {
        APIErrorResponse apiError = new APIErrorResponse(Collections.singletonList(ex.getMessage()));
        return buildResponseEntity(apiError, HttpStatus.NOT_IMPLEMENTED);
    }

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<Object> handleNotFoundException(Exception ex) {
        APIErrorResponse apiError = new APIErrorResponse(Collections.singletonList(ex.getMessage()));
        return buildResponseEntity(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidInputException.class)
    protected ResponseEntity<Object> handleInvalidInputException(InvalidInputException ex) {
        APIErrorResponse apiError = new APIErrorResponse(Collections.singletonList(ex.getMessage()));
        return buildResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }

    @Override
    @NotNull
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@NotNull MethodArgumentNotValidException ex,
                                                                  @NotNull HttpHeaders headers,
                                                                  @NotNull HttpStatus status,
                                                                  @NotNull WebRequest request) {
        return bindException(ex);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (ex instanceof BindException) {
            return bindException((BindException) ex);
        }
        APIErrorResponse apiErrorResponse = new APIErrorResponse(Collections.singletonList(ex.getMessage()));
        return buildResponseEntity(apiErrorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<Object> bindException(BindException bindException) {
        List<String> errors = new ArrayList<>();
        bindException.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.add(fieldName + ": " + errorMessage);
        });
        APIErrorResponse apiError = new APIErrorResponse(errors);
        return buildResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }
}