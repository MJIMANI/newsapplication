package ir.mjimani.news.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class InvalidInputException extends RuntimeException {

    private String message;

    public InvalidInputException(String message) {
        super(message);
        this.message = message;
    }

}