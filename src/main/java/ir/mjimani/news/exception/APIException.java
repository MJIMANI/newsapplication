package ir.mjimani.news.exception;

import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@NoArgsConstructor
public class APIException extends RuntimeException {

    private List<String> message = new ArrayList<>();

    public APIException(String message) {
        super(message);
        this.message = Collections.singletonList(message);
    }

}