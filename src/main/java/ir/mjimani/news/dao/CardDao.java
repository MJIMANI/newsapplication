package ir.mjimani.news.dao;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import ir.mjimani.news.controller.dto.card.CardListReqDto;
import ir.mjimani.news.controller.dto.card.UpdateCardReqDto;
import ir.mjimani.news.domain.Card;
import ir.mjimani.news.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class CardDao {

    private final MongoTemplate mongoTemplate;

    public Card createCard(Card card) {
        return mongoTemplate.save(card);
    }

    public Card getCard(String boardId, String cardId) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(Card.BFN.id.name()).is(cardId)
                        .and(Card.FN.boardId.name()).is(boardId)
        );

        Card card = mongoTemplate.findOne(query, Card.class);
        if (card == null) {
            throw new NotFoundException("Card not found.");
        }
        return card;
    }

    public Boolean updateCard(String boardId, String cardId, UpdateCardReqDto updateCardReqDto) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(Card.BFN.id.name()).is(cardId)
                        .and(Card.FN.boardId.name()).is(boardId)
        );

        Update update = new Update()
                .set(Card.FN.cardTitle.name(), updateCardReqDto.getCardTitle())
                .set(Card.BFN.modifiedOn.name(), new Date());

        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Card.class);
        if (updateResult.getMatchedCount() <= 0) {
            throw new NotFoundException("Card not found.");
        }

        return true;
    }

    public Boolean deleteCard(String boardId, String cardId) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(Card.BFN.id.name()).is(cardId)
                        .and(Card.FN.boardId.name()).is(boardId)
        );

        DeleteResult deleteResult = mongoTemplate.remove(query, Card.class);
        if (deleteResult.getDeletedCount() <= 0) {
            throw new NotFoundException("Card not found.");
        }
        return true;
    }

    public Boolean assignMember(String boardId, String cardId, String memberId) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(Card.BFN.id.name()).is(cardId)
                        .and(Card.FN.boardId.name()).is(boardId)
        );

        Update update = new Update()
                .addToSet(Card.FN.members.name(), memberId)
                .set(Card.BFN.modifiedOn.name(), new Date());

        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Card.class);
        if (updateResult.getMatchedCount() <= 0) {
            throw new NotFoundException("Card not found.");
        }

        return true;
    }

    public Boolean unAssignMember(String boardId, String cardId, String userId) {
        Query query = new Query();
        query.addCriteria(
                Criteria.where(Card.BFN.id.name()).is(cardId)
                        .and(Card.FN.boardId.name()).is(boardId)
        );

        Update update = new Update()
                .pull(Card.FN.members.name(), userId)
                .set(Card.BFN.modifiedOn.name(), new Date());

        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Card.class);
        if (updateResult.getMatchedCount() <= 0) {
            throw new NotFoundException("Card not found.");
        }

        return true;
    }

    public Page<Card> getCards(String boardId, CardListReqDto cardListReqDto, int page, int size) {
        Query query = new Query();
        Pageable pageable = PageRequest.of(page, size);
        Criteria criteria = Criteria.where(Card.FN.boardId.name()).is(boardId);

        if (!cardListReqDto.getCardTitle().isEmpty()) {
            criteria.and(Card.FN.cardTitle.name()).regex(cardListReqDto.getCardTitle());
        }

        if (!cardListReqDto.getMemberId().isEmpty()) {
            criteria.and(Card.FN.members.name()).is(cardListReqDto.getMemberId());
        }

        query
                .addCriteria(criteria)
                .with(pageable);

        if (cardListReqDto.getSortByModifiedOn()) {
            query.with(Sort.by(Sort.Direction.DESC, Card.BFN.modifiedOn.name()));
        }

        List<Card> cards = mongoTemplate.find(query, Card.class);

        long count = 0;
        if (!cards.isEmpty()) {
            count = mongoTemplate.count(query, Card.class);
        }

        return new PageImpl<>(cards, pageable, count);
    }

    public Boolean checkExistsByBoardId(String boardId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Card.FN.boardId.name()).is(boardId));

        return mongoTemplate.exists(query, Card.class);
    }
}
