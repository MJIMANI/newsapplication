package ir.mjimani.news.dao;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import ir.mjimani.news.controller.dto.board.UpdateBoardReqDto;
import ir.mjimani.news.domain.BaseDomain;
import ir.mjimani.news.domain.Board;
import ir.mjimani.news.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class BoardDao {
    private final MongoTemplate mongoTemplate;

    public Board createBoard(Board board) {
        return mongoTemplate.save(board);
    }

    public Page<Board> getList(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Query query = new Query()
                .with(pageable);
        List<Board> boards = mongoTemplate.find(query, Board.class);

        long count = 0;
        if (!boards.isEmpty()) {
            count = mongoTemplate.count(query, Board.class);
        }

        return new PageImpl<>(boards, pageable, count);
    }

    public Board getBoard(String boardId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Board.BFN.id.name()).is(boardId));

        Board board = mongoTemplate.findOne(query, Board.class);
        if (board == null) {
            throw new NotFoundException("Board not found.");
        }
        return board;
    }

    public Boolean updateBoard(String boardId, UpdateBoardReqDto updateBoardReqDto) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Board.BFN.id.name()).is(boardId));

        Update update = new Update()
                .set(Board.FN.boardName.name(), updateBoardReqDto.getBoardName())
                .set(BaseDomain.BFN.modifiedOn.name(), new Date());

        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Board.class);
        if (updateResult.getMatchedCount() <= 0) {
            throw new NotFoundException("Board not found.");
        }

        return true;
    }

    public Boolean deleteBoard(String boardId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Board.BFN.id.name()).is(boardId));

        DeleteResult deleteResult = mongoTemplate.remove(query, Board.class);
        if (deleteResult.getDeletedCount() <= 0) {
            throw new NotFoundException("Board not found.");
        }
        return true;
    }

    public Boolean checkExists(String boardId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Board.BFN.id.name()).is(boardId));
        return mongoTemplate.exists(query, Board.class);
    }

}
