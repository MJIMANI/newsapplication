package ir.mjimani.news.service;

import ir.mjimani.news.controller.dto.board.CreateBoardReqDto;
import ir.mjimani.news.controller.dto.board.CreateBoardResDto;
import ir.mjimani.news.controller.dto.board.UpdateBoardReqDto;
import ir.mjimani.news.dao.BoardDao;
import ir.mjimani.news.dao.CardDao;
import ir.mjimani.news.domain.Board;
import ir.mjimani.news.exception.APIException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BoardService {

    private final BoardDao boardDao;
    private final CardDao cardDao;

    public CreateBoardResDto createBoard(CreateBoardReqDto createBoardReqDto) {
        Board board = createBoardReqDto.toBoard();
        board.initCreatorIdAndCreatedDate();
        board = boardDao.createBoard(board);

        return CreateBoardResDto
                .builder()
                .boardId(board.getId())
                .build();
    }

    public Page<Board> getList(int page, int size) {
        return boardDao.getList(page, size);
    }

    public Board getBoard(String boardId) {
        return boardDao.getBoard(boardId);
    }

    public Boolean updateBoard(String boardId, UpdateBoardReqDto updateBoardReqDto) {
        return boardDao.updateBoard(boardId, updateBoardReqDto);
    }

    public Boolean deleteBoard(String boardId) {
        // Check cards
        Boolean cardExists = cardDao.checkExistsByBoardId(boardId);
        if (cardExists) {
            throw new APIException("Card exists, and you cannot delete board.");
        }
        return boardDao.deleteBoard(boardId);
    }
}
