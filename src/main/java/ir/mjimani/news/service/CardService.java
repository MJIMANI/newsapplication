package ir.mjimani.news.service;

import ir.mjimani.news.controller.dto.card.CardListReqDto;
import ir.mjimani.news.controller.dto.card.CreateCardReqDto;
import ir.mjimani.news.controller.dto.card.CreateCardResDto;
import ir.mjimani.news.controller.dto.card.UpdateCardReqDto;
import ir.mjimani.news.dao.BoardDao;
import ir.mjimani.news.dao.CardDao;
import ir.mjimani.news.domain.Card;
import ir.mjimani.news.exception.NotFoundException;
import ir.mjimani.news.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardService {

    private final BoardDao boardDao;
    private final CardDao cardDao;
    private final UserRepository userRepository;

    public CreateCardResDto createCard(String boardId, CreateCardReqDto createCardReqDto) {
        Boolean boardExists = boardDao.checkExists(boardId);
        if (!boardExists) {
            throw new NotFoundException("Board not found.");
        }

        Card card = createCardReqDto.toCard();
        card.setBoardId(boardId);
        card.initCreatorIdAndCreatedDate();

        card = cardDao.createCard(card);

        return new CreateCardResDto(card.getId());
    }

    public Card getCard(String boardId, String cardId) {
        return cardDao.getCard(boardId, cardId);
    }

    public Boolean updateCard(String boardId, String cardId, UpdateCardReqDto updateCardReqDto) {
        return cardDao.updateCard(boardId, cardId, updateCardReqDto);
    }

    public Boolean deleteCard(String boardId, String cardId) {
        return cardDao.deleteCard(boardId, cardId);
    }

    public Boolean assignMember(String boardId, String cardId, String userId) {
        checkUserExists(userId);

        return cardDao.assignMember(boardId, cardId, userId);
    }

    public Boolean unAssignMember(String boardId, String cardId, String userId) {
        checkUserExists(userId);

        return cardDao.unAssignMember(boardId, cardId, userId);
    }

    private void checkUserExists(String userId) {
        boolean userExists = userRepository.existsById(userId);
        if (!userExists) {
            throw new NotFoundException("User not found.");
        }
    }

    public Page<Card> getCards(String boardId, CardListReqDto cardListReqDto, int page, int size) {
        return cardDao.getCards(boardId, cardListReqDto, page, size);
    }

}
