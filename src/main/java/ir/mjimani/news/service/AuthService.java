package ir.mjimani.news.service;

import ir.mjimani.news.controller.dto.auth.LoginReqDto;
import ir.mjimani.news.controller.dto.auth.LoginResDto;
import ir.mjimani.news.controller.dto.auth.SignupReqDto;
import ir.mjimani.news.domain.User;
import ir.mjimani.news.enums.Role;
import ir.mjimani.news.exception.APIException;
import ir.mjimani.news.repository.UserRepository;
import ir.mjimani.news.security.JwtUtils;
import ir.mjimani.news.security.UserDetailsImp;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final AuthenticationManager authenticationManager;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final JwtUtils jwtUtils;

    public LoginResDto login(LoginReqDto loginReqDto) {

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginReqDto.getUsername(), loginReqDto.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImp userDetails = (UserDetailsImp) authentication.getPrincipal();

        return LoginResDto
                .builder()
                .id(userDetails.getId())
                .username(userDetails.getUsername())
                .accessToken(jwt)
                .tokenType("Bearer")
                .roles(userDetails.getAuthorities())
                .build();
    }

    public Boolean registerUser(SignupReqDto signUpReqDto) {
        if (userRepository.existsByUsername(signUpReqDto.getUsername())) {
            throw new APIException("Username is already taken!");
        }

        // Create new user's account
        User user = new User(signUpReqDto.getUsername(), passwordEncoder.encode(signUpReqDto.getPassword()));

        user.setRole(Role.ROLE_USER);
        userRepository.save(user);

        return true;
    }
}
