package ir.mjimani.news.security;

import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

    public static String getLoggedInUserId() {
        UserDetailsImp user = (UserDetailsImp) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getId();
    }
}
